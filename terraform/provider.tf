# provider.tf
# Configure the AWS Provider


provider "aws" {
  version = "~> 2.0"
  region  = "eu-west-2"

# Creds picked up externally. So not incliuded here.
}


# Terraform remote state file held on S3 for team access.
terraform {

  required_version = "~>0.11.14"

  backend "s3" { 
    region = "eu-west-2"
    bucket = "gavin-terraform-remote-state"
    key = "caci-project-gavin-terrarform-vpc-test.tfstate"
  }

  
}
