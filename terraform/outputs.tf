# outputs.tf

#########################################
## CI env. Outputs
#########################################

output "environment" {
  value = "${var.environment}"
}
output "region" {
  value = "${var.region}"
}



#########################################
## IAM Roles outputs
#########################################

output  "engineer_role_arn" {
    value = "${module.awsRoles.role_arn}"
}
output  "engineer_role_name" {
    value = "${module.awsRoles.role_name}"
}









