# variables.tf

variable "role_name" {
    description = "The role name"
}


variable "role_description" {
    description = "A description for the role."
}







variable "environment" {
  description = "Environment tag - dev/test/prod. Should match CI_ENVIRONMENT_NAME"
}

variable "tags" {
   description = "General tags to assign to the resource."
   type = "map"
}