# aws-roles.tf

resource "aws_iam_role" "role" {
  name = "${var.role_name}"
  description =  "${var.role_description}"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  

  tags = "${merge(map("purpose","${var.role_description}"), var.tags)}"
}