
# variables.tf

variable "region" {
    description = "London region eu-west-2, only"
    default = "eu-west-2"
}

variable "environment" {
    description = "'infra': AWS Terraform infrastructure incubation area. Or 'dev' Data science dev area, Or 'Test' integration test env." 
    default = "infra"
}

variable "project_name" {
    description = "The root/base name used for  various resources e.g. S3 bucket names, descriptions etc."
    default     = "gavin-vpc-test"
}


variable "tags" {
    description = "A map containing common key/value pair tags."
    type        = "map"
    default     = { 
        project     = "compN-vpc"   
        owner       = "Gavin"
        origin      = "Terraform created"
        repo        = "caci::git@bitbucket.org:gavinJLing/gavin-terraform.git"
    }
}



