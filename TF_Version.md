
## Which version of Terraform


```
$ terraform version
Terraform v0.11.13
+ provider.aws v2.43.0

Your version of Terraform is out of date! The latest version
is 0.12.18. You can update by downloading from www.terraform.io/downloads.html
```


I use `tfenv` to help control the Terraform  version, since I wish to migrate this project to latest Terraform (0.12.x)
```
$ brew install tfenv
```

When this is installed, I can then manage the version of TF that I have...

To discover what is available.
```
tfenv list-remote
0.12.18
0.12.17
0.12.16
0.12.15
...
0.12.0
0.11.15-oci
0.11.14
0.11.13
```

To install a  differnet version of Terraform
```
$ tfenv install 0.11.14
[INFO] Installing Terraform v0.11.14
[INFO] Downloading release tarball from https://releases.hashicorp.com/terraform/0.11.14/terraform_0.11.14_darwin_amd64.zip
...
[INFO] Installation of terraform v0.11.14 successful
[INFO] Switching to v0.11.14
[INFO] Switching completed
If you want to switch to a different version:

$ tfenv use 0.12.0
[INFO] Switching to v0.12.0
[INFO] Switching completed
```


## Environment Vars.
See official [docs](https://www.terraform.io/docs/commands/environment-variables.html). 


**TF_LOG**
Activate Terraform logging.... (try INFO, WARN, ERROR)
```
$ export TF_LOG=ERROR
```

To disable logging
```
$ expport TF_LOG=
```


